
<!DOCTYPE html>
<html lang="en">

<head>

  <title>VD - Proses Hitung</title>
  <?php $this->load->view("_partials/head.php") ?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $this->load->view("_partials/sidebar.php") ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php $this->load->view("_partials/topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Tables</h1>
          <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
			<a href="<?php echo site_url('listdata/add') ?>" class="btn btn-success btn-icon-split">
				<span class="icon text-white-50">
				    <i class="fas fa-plus"></i>
				</span>
			    <span class="text">Add</span>
			</a>
            <!-- <div class="my-2"></div> -->
            <br>
            <br>
            <!-- </div>
            <div class="card-body"> -->
            
            
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No ID</th>
                      <th>Nama Player</th>
                      <th>Kill</th>
                      <th>Death</th>
                      <th>Assist</th>
                      <th>Networth</th>
                      <th>Last Hit</th>
                      <th>GPM</th>
                      <th>XPM</th>
                      <th>Supp Cost</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No ID</th>
                      <th>Nama Player</th>
                      <th>Kill</th>
                      <th>Death</th>
                      <th>Assist</th>
                      <th>Networth</th>
                      <th>Last Hit</th>
                      <th>GPM</th>
                      <th>XPM</th>
                      <th>Supp Cost</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php 
						foreach ($player as $p): ?>
							<tr>
								<td style="text-align:justify">
									<?php echo $p->no_player ?>
								</td>
								<td style="text-align:justify">
									<?php echo $p->nama_player ?>
								</td>
                <td style="text-align:justify">
									<?php echo $p->r_killed ?>
								</td>
								<td style="text-align:justify">
									<?php echo $p->r_death ?>
								</td>
                <td style="text-align:justify">
								  <?php echo $p->r_assist ?>
								</td>
                <td style="text-align:justify">
									<?php echo $p->r_networth ?>
								</td>
                <td style="text-align:justify">
									<?php echo $p->r_last_hit ?>
								</td>
                <td style="text-align:justify">
									<?php echo $p->r_gpm ?>
								</td>
                <td style="text-align:justify">
									<?php echo $p->r_xpm ?>
								</td>
                <td style="text-align:justify">
									<?php echo $p->r_supp_cost ?>
								</td>
							</tr>
						<?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <?php $this->load->view("_partials/footer.php") ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->
  
  <?php $this->load->view("_partials/modal.php") ?>
  
  <?php $this->load->view("_partials/js.php") ?>

</body>

</html>

