<!DOCTYPE html>
<html lang="en">

<head>

  <title>SB Admin 2 - Tables</title>
  <?php $this->load->view("_partials/head.php") ?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <?php $this->load->view("_partials/sidebar.php") ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <?php $this->load->view("_partials/topbar.php") ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Tables</h1>
          <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
                <form class="user" method="post" enctype="multipart/form-data" action="<?php echo site_url('ListData/simpan/');?>">
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="nama_player" placeholder="Nama Player" required>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="text" class="form-control form-control-user" name="killed" placeholder="Killed" required>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-control-user" name="last_hit" placeholder="Last Hit" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="text" class="form-control form-control-user" name="death" placeholder="Death" required>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-control-user" name="gpm" placeholder="GPM" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="text" class="form-control form-control-user" name="assist" placeholder="Assist" required>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-control-user" name="xpm" placeholder="XPM" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="text" class="form-control form-control-user" name="networth" placeholder="Networth" required>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-control-user" name="supp_cost" placeholder="Supp Cost" required>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-user btn-block" type="submit" name="btn">Save</button>
                    <!-- <a href="login.html" class="btn btn-primary btn-user btn-block">Register Account</a> -->
                    <hr>
                    <!-- <a href="index.html" class="btn btn-google btn-user btn-block">
                    <i class="fab fa-google fa-fw"></i> Register with Google
                    </a>
                    <a href="index.html" class="btn btn-facebook btn-user btn-block">
                    <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                    </a> -->
                </form>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <?php $this->load->view("_partials/footer.php") ?>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->
  
  <?php $this->load->view("_partials/modal.php") ?>
  
  <?php $this->load->view("_partials/js.php") ?>

</body>

</html>



