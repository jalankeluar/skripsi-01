<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('Hasil_model');
    }

    public function index()
    {
        $r1 = $this->Hasil_model->count1();
        $r2 = $this->Hasil_model->count2();
        $r3 = $this->Hasil_model->count3();
        $r4 = $this->Hasil_model->count4();
        $r5 = $this->Hasil_model->count5();

        $data = array (
            'role1'   => $r1->jml,
            'role2'   => $r2->jml,
            'role3'   => $r3->jml,
            'role4'   => $r4->jml,
            'role5'   => $r5->jml,
        );
        $this->load->view("dashboard",$data);         
    }

}
