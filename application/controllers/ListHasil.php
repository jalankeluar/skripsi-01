<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListHasil extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Player_model");
        $this->load->model("Hitung_model");
        $this->load->model("Hasil_model");
        //load model admin
     //   $this->load->model('Login_model');
    }

    public function index()
    {
        $this->hitung1();
        $data["player"] = $this->Hasil_model->showData();
        $this->load->view("listHasil", $data);      
    }

    public function hitung1()
    {
        $player = $this->Hitung_model->getAll();
        

        foreach($player as $p){
            
            $d_no       = $p->no_player;
            $d_kill     = $p->r_killed;
            $d_death    = $p->r_death;
            $d_assist   = $p->r_assist;
            $d_networth = $p->r_networth;
            $d_lasthit  = $p->r_last_hit;
            $d_gpm      = $p->r_gpm;
            $d_xpm      = $p->r_xpm;
            $d_supp     = $p->r_supp_cost;

            $h_no       = $d_no;
            $h_kill     = $d_kill       * 0.3;
            $h_death    = $d_death      * 0.05;
            $h_assist   = $d_assist     * 0.15;
            $h_networth = $d_networth   * 0.15;
            $h_lasthit  = $d_lasthit    * 0.20;
            $h_gpm      = $d_gpm        * 0.05;
            $h_xpm      = $d_xpm        * 0.05;
            $h_supp     = $d_supp       * 0.05;
            $h_total    = ($h_kill + $h_death + $h_assist + $h_networth + $h_lasthit + $h_gpm + $h_xpm + $h_supp)*100;
            $h_role     = "";

            $h_tot = round($h_total);
            if($h_tot<=30){
                $h_role = "Support";
            }
            else if($h_tot>=31 && $h_tot<=40){
                $h_role = "Roam Support";
            }
            else if($h_tot>=41 && $h_tot<=55){
                $h_role = "Offlane";
            }
            else if($h_tot>=56 && $h_tot<=75){
                $h_role = "Midlane";
            }
            else if($h_tot>=76 && $h_tot<=100){
                $h_role = "Safelane";
            }

            $data = array(
                'no_player'   => $h_no,
                'h_killed'    => $h_kill,
                'h_death'     => $h_death,
                'h_assist'    => $h_assist,
                'h_networth'  => $h_networth,
                'h_lh'        => $h_lasthit,
                'h_gpm'       => $h_gpm,
                'h_xpm'       => $h_xpm,
                'h_supp_cost' => $h_supp,
                'h_total'     => $h_total,
                'h_role'      => $h_role,
            );

            print_r($data);
            
            $this->Hasil_model->update($h_no,$data);
        }
    }

}
