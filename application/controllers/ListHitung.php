<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListHitung extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Player_model");
        $this->load->model("Hitung_model");
        //load model admin
     //   $this->load->model('Login_model');
    }

    public function index()
    {
        $this->hitung1();
        $data["player"] = $this->Hitung_model->showData();
        $this->load->view("listHitung", $data);      
    }

    public function hitung1()
    {
        $player = $this->Player_model->getAll();

        foreach($player as $p){
            
            $d_no       = $p->no_player;
            $d_nama     = $p->nama_player;
            $d_kill     = $p->killed;
            $d_death    = $p->death;
            $d_assist   = $p->assist;
            $d_networth = $p->networth;
            $d_lasthit  = $p->last_hit;
            $d_gpm      = $p->gpm;
            $d_xpm      = $p->xpm;
            $d_supp     = $p->supp_cost;


            $h_no       = $d_no;
            $h_nama     = $d_nama;
            $h_kill     = $this->Hitung_model->R1($d_no);
            $h_death    = $this->Hitung_model->R2($d_no);
            $h_assist   = $this->Hitung_model->R3($d_no);
            $h_networth = $this->Hitung_model->R4($d_no);
            $h_lasthit  = $this->Hitung_model->R5($d_no);
            $h_gpm      = $this->Hitung_model->R6($d_no);
            $h_xpm      = $this->Hitung_model->R7($d_no);
            $h_supp     = $this->Hitung_model->R8($d_no);

            $data = array(
                'no_player'   => $h_no,
                'r_killed'    => $h_kill,
                'r_death'     => $h_death,
                'r_assist'    => $h_assist,
                'r_networth'  => $h_networth,
                'r_last_hit'  => $h_lasthit,
                'r_gpm'       => $h_gpm,
                'r_xpm'       => $h_xpm,
                'r_supp_cost' => $h_supp,
            );
            
            $this->Hitung_model->update($h_no,$data);
        }
    }

}
