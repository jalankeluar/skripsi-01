<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Initial extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data_siswa = $this->db->query("SELECT * FROM data_siswa")->result();
        $data_kriteria = $this->db->query("SELECT * FROM nilai_bobot")->result();

        // $akademik = $data_kriteria[0]->nilai;
        // $sosial   = $data_kriteria[1]->nilai;
        // $absensi  = $data_kriteria[2]->nilai;

        // $total;
        // $nilai_bobot = array();
        // $panjang     = count($data_kriteria);
        // for ($i=0; $i < $panjang; $i++) {         
        //     $nilai = array();
        //     $bobot = 0;
        //     for ($j=0; $j < $panjang ; $j++) { 
        //         $bobot = $data_kriteria[$j]->nilai / $data_kriteria[$i]->nilai;
        //         array_push($nilai,$bobot);
        //     }
        //     // array_push($nilai,$total);
        //     array_push($nilai_bobot,$nilai);
        // }
        
        $panjang     = count($data_kriteria);
        $nilai_bobot = array();
        $nilai_bobot[0][0] = 1;
        $nilai_bobot[0][1] = 5;
        $nilai_bobot[0][2] = 3;
        $nilai_bobot[1][0] = 0.2;
        $nilai_bobot[1][1] = 1;
        $nilai_bobot[1][2] = 0.333333333;
        $nilai_bobot[2][0] = 0.333333333;
        $nilai_bobot[2][1] = 3;
        $nilai_bobot[2][2] = 1;


        $nilai_total = array();
        for ($i=0; $i < $panjang ; $i++) { 
            $total = 0;
            for ($j=0; $j < $panjang ; $j++) { 
                $total = $nilai_bobot[$j][$i] + $total;
            }
            
            array_push($nilai_total,$total);
        }

        //normalisasi
        $normalisasi = array();

        for ($i=0; $i < $panjang; $i++) {         
            $norm_nilai = array();
            $total = 0;
            for ($j=0; $j < $panjang ; $j++) {
                // var_dump($nilai_bobot[$i][$j]);
                // var_dump($nilai_total[$j]);
                $bobot = $nilai_bobot[$i][$j] / $nilai_total[$j];
                array_push($norm_nilai,$bobot);
                $total = $total + $bobot;
            }
            $rata = $total/$panjang;
            array_push($norm_nilai,$rata);
            // array_push($nilai,$total);
            array_push($normalisasi,$norm_nilai);
        }

        $data["norm_kriteria"] = $normalisasi;
        $this->load->view("listData", $data);      
    }

    public function proses_akademik()
    {
        $data_akademik = $this->db->query("SELECT nilai_akademik FROM data_siswa")->result();

        $akademik = array();
        foreach ($data_akademik as $dak) {
            array_push($akademik, $dak->nilai_akademik);
        }

        $panjang     = count($akademik);
        $nilai_akademik = array();
        for ($i=0; $i < $panjang; $i++) {         
            $nilai = array();
            $total = 0;
            
            // var_dump('---------------------------------');
            for ($j=0; $j < $panjang ; $j++) {
                $selisih = $akademik[$i] - $akademik[$j];
                if($selisih > 0){
                    if($selisih == 0){
                        $bobot = 1;
                    }else if($selisih >= 1 && $selisih <= 10){
                        $bobot = 2;
                    }
                    else if($selisih >= 11 && $selisih <= 20){
                        $bobot = 3;
                    }
                    else if($selisih >= 21 && $selisih <= 50){
                        $bobot = 5;
                    }
                    else if($selisih >= 51 && $selisih <= 60){
                        $bobot = 7;
                    }
                    else if($selisih >= 61 && $selisih <= 80){
                        $bobot = 9;
                    }
                }
                else{
                    if($selisih == 0){
                        $bobot = 1/1;
                    }else if($selisih <= -1 && $selisih >= -10){
                        $bobot = 1/2;
                    }
                    else if($selisih <= -11 && $selisih >= -20){
                        $bobot = 1/3;
                    }
                    else if($selisih <= -21 && $selisih >= -50){
                        $bobot = 1/5;
                    }
                    else if($selisih <= -51 && $selisih >= -60){
                        $bobot = 1/7;
                    }
                    else if($selisih <= -61 && $selisih >= -80){
                        $bobot = 1/9;
                    }
                }

                // var_dump($bobot);
                // var_dump('<br>');
                array_push($nilai,(float)$bobot);
            }
            array_push($nilai_akademik,$nilai);
        }

        $total_akademik = array();
        for ($i=0; $i < $panjang ; $i++) { 
            $total = 0;
            for ($j=0; $j < $panjang ; $j++) { 
                $total = $nilai_akademik[$j][$i] + $total;
            }
            array_push($total_akademik,$total);
        }

        
        var_dump("<pre>");
        print_r($panjang);
        var_dump("</pre>");
        $normalisasi_akademik = array();

        for ($i=0; $i < $panjang; $i++) {         
            $norm_nilai = array();
            $total = 0;
            for ($j=0; $j < $panjang ; $j++) {
                // var_dump($nilai_bobot[$i][$j]);
                // var_dump($nilai_total[$j]);
                $bobot = $nilai_akademik[$i][$j] / $total_akademik[$j];
                array_push($norm_nilai,$bobot);
                $total = $total + $bobot;
            }
            array_push($norm_nilai,$total);
            $rata = $total/$panjang;
            array_push($norm_nilai,$rata);
            // array_push($nilai,$total);
            array_push($normalisasi_akademik,$norm_nilai);
        }
        var_dump("<pre>");
        print_r($normalisasi_akademik);
        var_dump("</pre>");
        die();


    }

    public function proses_sosial()
    {
        $data_sosial = $this->db->query("SELECT nilai_sosial FROM data_siswa")->result();

        $sosial = array();
        foreach ($data_sosial as $dak) {
            array_push($sosial, $dak->nilai_sosial);
        }

        $panjang     = count($sosial);
        $nilai_sosial = array();
        for ($i=0; $i < $panjang; $i++) {         
            $nilai = array();
            $total = 0;
            
            // var_dump('---------------------------------');
            for ($j=0; $j < $panjang ; $j++) {
                $selisih = $sosial[$i] - $sosial[$j];
                if($selisih > 0){
                    if($selisih == 0){
                        $bobot = 1;
                    }else if($selisih >= 1 && $selisih <= 10){
                        $bobot = 2;
                    }
                    else if($selisih >= 11 && $selisih <= 20){
                        $bobot = 3;
                    }
                    else if($selisih >= 21 && $selisih <= 50){
                        $bobot = 5;
                    }
                    else if($selisih >= 51 && $selisih <= 60){
                        $bobot = 7;
                    }
                    else if($selisih >= 61 && $selisih <= 80){
                        $bobot = 9;
                    }
                }
                else{
                    if($selisih == 0){
                        $bobot = 1/1;
                    }else if($selisih <= -1 && $selisih >= -10){
                        $bobot = 1/2;
                    }
                    else if($selisih <= -11 && $selisih >= -20){
                        $bobot = 1/3;
                    }
                    else if($selisih <= -21 && $selisih >= -50){
                        $bobot = 1/5;
                    }
                    else if($selisih <= -51 && $selisih >= -60){
                        $bobot = 1/7;
                    }
                    else if($selisih <= -61 && $selisih >= -80){
                        $bobot = 1/9;
                    }
                }

                // var_dump($bobot);
                // var_dump('<br>');
                array_push($nilai,(float)$bobot);
            }
            array_push($nilai_sosial,$nilai);
        }

        $total_sosial = array();
        for ($i=0; $i < $panjang ; $i++) { 
            $total = 0;
            for ($j=0; $j < $panjang ; $j++) { 
                $total = $nilai_sosial[$j][$i] + $total;
            }
            array_push($total_sosial,$total);
        }

        
        var_dump("<pre>");
        print_r($panjang);
        var_dump("</pre>");
        $normalisasi_sosial = array();

        for ($i=0; $i < $panjang; $i++) {         
            $norm_nilai = array();
            $total = 0;
            for ($j=0; $j < $panjang ; $j++) {
                // var_dump($nilai_bobot[$i][$j]);
                // var_dump($nilai_total[$j]);
                $bobot = $nilai_sosial[$i][$j] / $total_sosial[$j];
                array_push($norm_nilai,$bobot);
                $total = $total + $bobot;
            }
            array_push($norm_nilai,$total);
            $rata = $total/$panjang;
            array_push($norm_nilai,$rata);
            // array_push($nilai,$total);
            array_push($normalisasi_sosial,$norm_nilai);
        }
        var_dump("<pre>");
        print_r($normalisasi_sosial);
        var_dump("</pre>");
        die();


    }

    public function proses_absensi()
    {
        $data_absensi = $this->db->query("SELECT nilai_absensi FROM data_siswa")->result();

        $absensi = array();
        foreach ($data_absensi as $dak) {
            array_push($absensi, $dak->nilai_absensi);
        }

        $panjang     = count($absensi);
        $nilai_absensi = array();
        for ($i=0; $i < $panjang; $i++) {         
            $nilai = array();
            $total = 0;
            
            // var_dump('---------------------------------');
            for ($j=0; $j < $panjang ; $j++) {
                $selisih = $absensi[$i] - $absensi[$j];
                if($selisih > 0){
                    if($selisih == 0){
                        $bobot = 1;
                    }else if($selisih >= 1 && $selisih <= 10){
                        $bobot = 2;
                    }
                    else if($selisih >= 11 && $selisih <= 20){
                        $bobot = 3;
                    }
                    else if($selisih >= 21 && $selisih <= 50){
                        $bobot = 5;
                    }
                    else if($selisih >= 51 && $selisih <= 60){
                        $bobot = 7;
                    }
                    else if($selisih >= 61 && $selisih <= 80){
                        $bobot = 9;
                    }
                }
                else{
                    if($selisih == 0){
                        $bobot = 1/1;
                    }else if($selisih <= -1 && $selisih >= -10){
                        $bobot = 1/2;
                    }
                    else if($selisih <= -11 && $selisih >= -20){
                        $bobot = 1/3;
                    }
                    else if($selisih <= -21 && $selisih >= -50){
                        $bobot = 1/5;
                    }
                    else if($selisih <= -51 && $selisih >= -60){
                        $bobot = 1/7;
                    }
                    else if($selisih <= -61 && $selisih >= -80){
                        $bobot = 1/9;
                    }
                }

                // var_dump($bobot);
                // var_dump('<br>');
                array_push($nilai,(float)$bobot);
            }
            array_push($nilai_absensi,$nilai);
        }

        $total_absensi = array();
        for ($i=0; $i < $panjang ; $i++) { 
            $total = 0;
            for ($j=0; $j < $panjang ; $j++) { 
                $total = $nilai_absensi[$j][$i] + $total;
            }
            array_push($total_absensi,$total);
        }

        
        var_dump("<pre>");
        print_r($panjang);
        var_dump("</pre>");
        $normalisasi_absensi = array();

        for ($i=0; $i < $panjang; $i++) {         
            $norm_nilai = array();
            $total = 0;
            for ($j=0; $j < $panjang ; $j++) {
                // var_dump($nilai_bobot[$i][$j]);
                // var_dump($nilai_total[$j]);
                $bobot = $nilai_absensi[$i][$j] / $total_absensi[$j];
                array_push($norm_nilai,$bobot);
                $total = $total + $bobot;
            }
            array_push($norm_nilai,$total);
            $rata = $total/$panjang;
            array_push($norm_nilai,$rata);
            // array_push($nilai,$total);
            array_push($normalisasi_absensi,$norm_nilai);
        }
        var_dump("<pre>");
        print_r($normalisasi_absensi);
        var_dump("</pre>");
        die();


    }



    //============================================================== trash ========================================
    public function add()
    {
        $this->load->view("add");
    }

    public function simpan()
    {
        $data = array(
            'no_player'      => $this->Player_model->cekID(),
            'nama_player'    => $this->input->post('nama_player'),
            'killed'         => $this->input->post('killed'),
            'death'          => $this->input->post('death'),
            'assist'         => $this->input->post('assist'),
            'networth'       => $this->input->post('networth'),
            'last_hit'       => $this->input->post('last_hit'),
            'gpm'            => $this->input->post('gpm'),
            'xpm'            => $this->input->post('xpm'),
            'supp_cost'      => $this->input->post('supp_cost'),
        );

        $no = $data['no_player'];

        $this->Player_model->save($data);
        $this->hitung1($no);
        $this->hitung2($no);
        redirect(site_url('listdata'));
    }

    public function hitung1($x)
    {
            $pemain = $this->Player_model->getById($x);

            $d_no       = $pemain->no_player;
            $d_nama     = $pemain->nama_player;
            $d_kill     = $pemain->killed;
            $d_death    = $pemain->death;
            $d_assist   = $pemain->assist;
            $d_networth = $pemain->networth;
            $d_lasthit  = $pemain->last_hit;
            $d_gpm      = $pemain->gpm;
            $d_xpm      = $pemain->xpm;
            $d_supp     = $pemain->supp_cost;


            $h_no       = $d_no;
            $h_nama     = $d_nama;
            $h_kill     = $this->Hitung_model->R1($d_no);
            $h_death    = $this->Hitung_model->R2($d_no);
            $h_assist   = $this->Hitung_model->R3($d_no);
            $h_networth = $this->Hitung_model->R4($d_no);
            $h_lasthit  = $this->Hitung_model->R5($d_no);
            $h_gpm      = $this->Hitung_model->R6($d_no);
            $h_xpm      = $this->Hitung_model->R7($d_no);
            $h_supp     = $this->Hitung_model->R8($d_no);
            
            

            $data = array(
                'no_player'   => $x,
                'r_killed'    => $h_kill,
                'r_death'     => $h_death,
                'r_assist'    => $h_assist,
                'r_networth'  => $h_networth,
                'r_last_hit'  => $h_lasthit,
                'r_gpm'       => $h_gpm,
                'r_xpm'       => $h_xpm,
                'r_supp_cost' => $h_supp,
            );

            
            // var_dump('<br><br>');
            // var_dump($data);
            
            // var_dump('<br><br>');
            
            $this->Hitung_model->save($data);
    }

    public function hitung2($x)
    {
        
            $slay = $this->Hitung_model->getById($x);

            $d_no       = $slay->no_player;
            $d_kill     = $slay->r_killed;
            $d_death    = $slay->r_death;
            $d_assist   = $slay->r_assist;
            $d_networth = $slay->r_networth;
            $d_lasthit  = $slay->r_last_hit;
            $d_gpm      = $slay->r_gpm;
            $d_xpm      = $slay->r_xpm;
            $d_supp     = $slay->r_supp_cost;


            $h_no       = $d_no;
            $h_kill     = $d_kill       * 0.3;
            $h_death    = $d_death      * 0.05;
            $h_assist   = $d_assist     * 0.15;
            $h_networth = $d_networth   * 0.15;
            $h_lasthit  = $d_lasthit    * 0.20;
            $h_gpm      = $d_gpm        * 0.05;
            $h_xpm      = $d_xpm        * 0.05;
            $h_supp     = $d_supp       * 0.05;
            $h_total    = ($h_kill + $h_death + $h_assist + $h_networth + $h_lasthit + $h_gpm + $h_xpm + $h_supp)*100;
            $h_tot      = $h_total      * 100;
            $h_role     = "";
            
            $h_tot = round($h_total);
            if($h_tot<=30){
                $h_role = "Support";
            }
            else if($h_tot>=31 && $h_tot<=40){
                $h_role = "Roam Support";
            }
            else if($h_tot>=41 && $h_tot<=55){
                $h_role = "Offlane";
            }
            else if($h_tot>=56 && $h_tot<=75){
                $h_role = "Midlane";
            }
            else if($h_tot>=76 && $h_tot<=100){
                $h_role = "Safelane";
            }

            $data = array(
                'no_player'   => $x,
                'h_killed'    => $h_kill,
                'h_death'     => $h_death,
                'h_assist'    => $h_assist,
                'h_networth'  => $h_networth,
                'h_lh'        => $h_lasthit,
                'h_gpm'       => $h_gpm,
                'h_xpm'       => $h_xpm,
                'h_supp_cost' => $h_supp,
                'h_total'     => $h_total,
                'h_role'      => $h_role,
            );
            
            $this->Hasil_model->save($data);
    }

}
