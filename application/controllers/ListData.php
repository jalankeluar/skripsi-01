<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListData extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Player_model");
        $this->load->model("Hitung_model");
        $this->load->model("Hasil_model");
        //load model admin
     //   $this->load->model('Login_model');
    }

    public function index()
    {
        $data["player"] = $this->Player_model->getAll();
        $this->load->view("listData", $data);      
    }

    public function add()
    {
        $this->load->view("add");
    }

    public function simpan()
    {
        $data = array(
            'no_player'      => $this->Player_model->cekID(),
            'nama_player'    => $this->input->post('nama_player'),
            'killed'         => $this->input->post('killed'),
            'death'          => $this->input->post('death'),
            'assist'         => $this->input->post('assist'),
            'networth'       => $this->input->post('networth'),
            'last_hit'       => $this->input->post('last_hit'),
            'gpm'            => $this->input->post('gpm'),
            'xpm'            => $this->input->post('xpm'),
            'supp_cost'      => $this->input->post('supp_cost'),
        );

        $no = $data['no_player'];

        $this->Player_model->save($data);
        $this->hitung1($no);
        $this->hitung2($no);
        redirect(site_url('listdata'));
    }

    public function hitung1($x)
    {
            $pemain = $this->Player_model->getById($x);

            $d_no       = $pemain->no_player;
            $d_nama     = $pemain->nama_player;
            $d_kill     = $pemain->killed;
            $d_death    = $pemain->death;
            $d_assist   = $pemain->assist;
            $d_networth = $pemain->networth;
            $d_lasthit  = $pemain->last_hit;
            $d_gpm      = $pemain->gpm;
            $d_xpm      = $pemain->xpm;
            $d_supp     = $pemain->supp_cost;


            $h_no       = $d_no;
            $h_nama     = $d_nama;
            $h_kill     = $this->Hitung_model->R1($d_no);
            $h_death    = $this->Hitung_model->R2($d_no);
            $h_assist   = $this->Hitung_model->R3($d_no);
            $h_networth = $this->Hitung_model->R4($d_no);
            $h_lasthit  = $this->Hitung_model->R5($d_no);
            $h_gpm      = $this->Hitung_model->R6($d_no);
            $h_xpm      = $this->Hitung_model->R7($d_no);
            $h_supp     = $this->Hitung_model->R8($d_no);
            
            

            $data = array(
                'no_player'   => $x,
                'r_killed'    => $h_kill,
                'r_death'     => $h_death,
                'r_assist'    => $h_assist,
                'r_networth'  => $h_networth,
                'r_last_hit'  => $h_lasthit,
                'r_gpm'       => $h_gpm,
                'r_xpm'       => $h_xpm,
                'r_supp_cost' => $h_supp,
            );

            
            // var_dump('<br><br>');
            // var_dump($data);
            
            // var_dump('<br><br>');
            
            $this->Hitung_model->save($data);
    }

    public function hitung2($x)
    {
        
            $slay = $this->Hitung_model->getById($x);

            $d_no       = $slay->no_player;
            $d_kill     = $slay->r_killed;
            $d_death    = $slay->r_death;
            $d_assist   = $slay->r_assist;
            $d_networth = $slay->r_networth;
            $d_lasthit  = $slay->r_last_hit;
            $d_gpm      = $slay->r_gpm;
            $d_xpm      = $slay->r_xpm;
            $d_supp     = $slay->r_supp_cost;


            $h_no       = $d_no;
            $h_kill     = $d_kill       * 0.3;
            $h_death    = $d_death      * 0.05;
            $h_assist   = $d_assist     * 0.15;
            $h_networth = $d_networth   * 0.15;
            $h_lasthit  = $d_lasthit    * 0.20;
            $h_gpm      = $d_gpm        * 0.05;
            $h_xpm      = $d_xpm        * 0.05;
            $h_supp     = $d_supp       * 0.05;
            $h_total    = ($h_kill + $h_death + $h_assist + $h_networth + $h_lasthit + $h_gpm + $h_xpm + $h_supp)*100;
            $h_tot      = $h_total      * 100;
            $h_role     = "";
            
            $h_tot = round($h_total);
            if($h_tot<=30){
                $h_role = "Support";
            }
            else if($h_tot>=31 && $h_tot<=40){
                $h_role = "Roam Support";
            }
            else if($h_tot>=41 && $h_tot<=55){
                $h_role = "Offlane";
            }
            else if($h_tot>=56 && $h_tot<=75){
                $h_role = "Midlane";
            }
            else if($h_tot>=76 && $h_tot<=100){
                $h_role = "Safelane";
            }

            $data = array(
                'no_player'   => $x,
                'h_killed'    => $h_kill,
                'h_death'     => $h_death,
                'h_assist'    => $h_assist,
                'h_networth'  => $h_networth,
                'h_lh'        => $h_lasthit,
                'h_gpm'       => $h_gpm,
                'h_xpm'       => $h_xpm,
                'h_supp_cost' => $h_supp,
                'h_total'     => $h_total,
                'h_role'      => $h_role,
            );
            
            $this->Hasil_model->save($data);
    }

}
