<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Hasil_model extends CI_Model
    {
        private $_table = "m_hasil";
    
        public $no_player;
        public $h_killed;
        public $h_death;
        public $h_assist; 
        public $h_networth;
        public $h_lh;
        public $h_gpm;
        public $h_xpm;
        public $h_supp_cost;
        public $h_total;
        public $h_role;

        public function getAll()
        {
            return $this->db->get($this->_table)->result();
        }

        public function showData()
        {
            return $this->db->query("SELECT b.no_player,
            h_killed,
            h_death,
            h_assist,
            h_networth,
            h_lh,
            h_gpm,
            h_xpm,
            h_supp_cost,
            h_total,
            h_role,
            a.nama_player
             from m_hasil b LEFT JOIN m_player a on a.no_player = b.no_player")->result();
        }
        
        public function getById($id)
        {
            return $this->db->query("SELECT *
            FROM m_player            
            WHERE no_player = '$id'")->row();
        }

        public function count1()
        {
            return $this->db->query("SELECT COUNT(h_role) as jml
            FROM m_hasil
            WHERE h_role = 'Safelane'")->row();
        }

        public function count2()
        {
            return $this->db->query("SELECT COUNT(h_role) as jml
            FROM m_hasil
            WHERE h_role = 'Midlane'")->row();
        }

        public function count3()
        {
            return $this->db->query("SELECT COUNT(h_role) as jml
            FROM m_hasil
            WHERE h_role = 'Offlane'")->row();
        }

        public function count4()
        {
            return $this->db->query("SELECT COUNT(h_role) as jml
            FROM m_hasil
            WHERE h_role = 'Roam Support'")->row();
        }

        public function count5()
        {
            return $this->db->query("SELECT COUNT(h_role) as jml
            FROM m_hasil
            WHERE h_role = 'Support'")->row();
        }

        public function R1($x)
        {
            $hasil = $this->db->query("SELECT (b.killed / a.maks) as jawab from
            (SELECT MAX(killed) as maks FROM m_player) a,
            (SELECT killed FROM m_player WHERE no_player = '".$x."') b")->row();
            
            return $hasil->jawab;
        }

        public function R2($x)
        {
            
            $hasil = $this->db->query("SELECT (a.mins / b.death) as jawab from
            (SELECT MIN(death) as mins FROM m_player) a,
            (SELECT death FROM m_player WHERE no_player = '".$x."') b")->row();

            return $hasil->jawab;
        }

        public function R3($x)
        {
            $hasil = $this->db->query("SELECT (b.assist / a.maks) as jawab from
            (SELECT MAX(assist) as maks FROM m_player) a,
            (SELECT assist FROM m_player WHERE no_player = '".$x."') b")->row();


            return $hasil->jawab;
        }

        public function R4($x)
        {
            $hasil = $this->db->query("SELECT (b.networth / a.maks) as jawab from
            (SELECT MAX(networth) as maks FROM m_player) a,
            (SELECT networth FROM m_player WHERE no_player = '".$x."') b")->row();


            return $hasil->jawab;
        }
  
        public function R5($x)
        {
            $hasil = $this->db->query("SELECT (b.last_hit / a.maks) as jawab from
            (SELECT MAX(last_hit) as maks FROM m_player) a,
            (SELECT last_hit FROM m_player WHERE no_player = '".$x."') b")->row();

            return $hasil->jawab;
        }
        
        public function R6($x)
        {
            $hasil = $this->db->query("SELECT (b.gpm / a.maks) as jawab from
            (SELECT MAX(gpm) as maks FROM m_player) a,
            (SELECT gpm FROM m_player WHERE no_player = '".$x."') b")->row();

            return $hasil->jawab;
        }

        public function R7($x)
        {
            $hasil = $this->db->query("SELECT (b.xpm / a.maks) as jawab from
            (SELECT MAX(xpm) as maks FROM m_player) a,
            (SELECT xpm FROM m_player WHERE no_player = '".$x."') b")->row();


            return $hasil->jawab;
        }
        
        public function R8($x)
        {
            $hasil = $this->db->query("SELECT (b.supp_cost / a.maks) as jawab from
            (SELECT MAX(supp_cost) as maks FROM m_player) a,
            (SELECT supp_cost FROM m_player WHERE no_player = '".$x."') b")->row();

            return $hasil->jawab;
        }

        public function cekID(){
            $akhir = $this->db->select('no_player')->order_by('no_player','desc')->limit(1)->get($this->_table)->row('no_player');            
            $hasil="";
            // var_dump($akhir);
    
            if(!empty($akhir)){
                $kata  = substr($akhir,4,strlen($akhir));
                // var_dump($kata);
                $kata2  = substr($akhir,0,4);
    
                $angka = sprintf("%03d", $kata+1);
                // var_dump($apa);
                $hasil = $kata2 . $angka;
                // var_dump($hasil);
    
                return $hasil;
            }else{
                $hasil = "DOTA001";
                return $hasil;
            }
        }
        
        public function save($data)
        {
            $this->db->insert("m_hasil",$data);
        }
        
        public function update($no,$data)
        {
            $this->db->where("no_player",$no);
            $this->db->update("m_hasil",$data);
        }
    }
?>
