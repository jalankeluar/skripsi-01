<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Player_model extends CI_Model
    {
        private $_table = "m_player";
    
        public $no_player;
        public $nama_player;
        public $killed;
        public $death;
        public $assist; 
        public $networth;
        public $last_hit;
        public $gpm;
        public $xpm;
        public $supp_cost;

        public function getAll()
        {
            return $this->db->get($this->_table)->result();
        }
        
        public function getById($id)
        {
            return $this->db->query("SELECT *
            FROM m_player            
            WHERE no_player = '$id'")->row();
        }

        public function cekID(){
            $akhir = $this->db->select('no_player')->order_by('no_player','desc')->limit(1)->get($this->_table)->row('no_player');            
            $hasil="";
            // var_dump($akhir);
    
            if(!empty($akhir)){
                $kata  = substr($akhir,4,strlen($akhir));
                // var_dump($kata);
                $kata2  = substr($akhir,0,4);
    
                $angka = sprintf("%03d", $kata+1);
                // var_dump($apa);
                $hasil = $kata2 . $angka;
                // var_dump($hasil);
    
                return $hasil;
            }else{
                $hasil = "DOTA001";
                return $hasil;
            }
        }

        
        public function save($data)
        {
            $this->db->insert('m_player',$data);
        }
    }
?>
